#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "cpu/util/CPUUtil.h"
#include "gpu/GPUKernel.cuh"
#include <iostream>
#include <cstdio>
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

__global__ void CudaMain(BlockOfNeurons* all_buffers, int buffer_id);

static void handleCUDAError(cudaError_t err, const char *file, int line)
{
	if (err != cudaSuccess) {
		printf("%s in %s at line %d\n", cudaGetErrorString(err), file, line);
		std::string exit;
		std::cin >> exit;
	}
}

#define CUDA_ERROR_CHECK(err) (handleCUDAError(err, __FILE__, __LINE__ ))
#define ITERATIONS 1000000
#define BUFFER_RUNNING 0
#define BUFFER_READY BUFFER_RUNNING + 1
#define BUFFER_UPLOADING BUFFER_RUNNING + 2
#define BUFFER_DOWNLOADING BUFFER_RUNNING + 3
#define BUFFER_EMPTY BUFFER_RUNNING + 4
#define BUFFER_FINISHED BUFFER_RUNNING + 5
#define NUM_BUFFERS 4

int main(int argc, char*argv[])
{
	static const int NUM_STREAMS = 4 + 1;
	int devCount;
	CUDA_ERROR_CHECK(cudaGetDeviceCount(&devCount));
	printf("CUDA Device Query...\n");
	printf("There are %d CUDA devices.\n", devCount);
	MainData cpuData = CPUUtil::Setup(1024, 4);

	char *buffer_states = new char[NUM_BUFFERS];

	//Fill the buffer flags
	for (int i = 0; i < NUM_BUFFERS; i++)
	{
		buffer_states[i] = BUFFER_EMPTY;
	};

	//Setup blocks data transfer
	BlockOfNeurons *h_blocks_buffers;
	BlockOfNeurons *dev_blocks_buffers;
	
	//Alocate buffers
	CUDA_ERROR_CHECK(cudaHostAlloc(&h_blocks_buffers, sizeof(BlockOfNeurons) * cpuData.NUM_BLOCKS_DEV * NUM_BUFFERS, cudaHostAllocDefault));

	//Alocate on device
	CUDA_ERROR_CHECK(cudaMalloc(&dev_blocks_buffers, sizeof(BlockOfNeurons) * cpuData.NUM_BLOCKS_DEV * NUM_BUFFERS));

	//create streams to run the kernel in and the coping in
	cudaStream_t streams[NUM_STREAMS];
	for (int i = 0; i < NUM_STREAMS; i++)
	{
		CUDA_ERROR_CHECK(cudaStreamCreate(&streams[i]));
	}

	for (int i = 0; i < ITERATIONS;)
	{
		for (int j = 0; j < NUM_BUFFERS; j++)
		{
			char state = buffer_states[j];

			//Calculate the offset of the buffer in the main array
			int buffer_offset = j * cpuData.NUM_BLOCKS_DEV;

			if (state == BUFFER_EMPTY)
			{
				//Set the state of the buffer to uploading
				buffer_states[j] = BUFFER_UPLOADING;

				//Copy over the buffer data
				CUDA_ERROR_CHECK(cudaMemcpyAsync(h_blocks_buffers + buffer_offset, dev_blocks_buffers + buffer_offset, sizeof(BlockOfNeurons) * cpuData.NUM_BLOCKS_DEV, cudaMemcpyHostToDevice, streams[j]));
			}
			else if (state == BUFFER_FINISHED)
			{
				//Set the state of the buffer to downloading
				buffer_states[j] = BUFFER_DOWNLOADING;

				//Copy over the buffer data
				CUDA_ERROR_CHECK(cudaMemcpyAsync(h_blocks_buffers + buffer_offset, dev_blocks_buffers + buffer_offset, sizeof(BlockOfNeurons) * cpuData.NUM_BLOCKS_DEV, cudaMemcpyDeviceToHost, streams[j]));
			}
			else if (state == BUFFER_DOWNLOADING)
			{
				if (cudaStreamQuery(streams[j]) == cudaSuccess)
				{
					//Set the state of the buffer to empty
					buffer_states[j] = BUFFER_EMPTY;
					i++;
					if (i % 1000 == 0)
					{
						printf("On iteration: %d\n", i);
					}
				}
			}
			else if (state == BUFFER_UPLOADING)
			{
				if (cudaStreamQuery(streams[j]) == cudaSuccess)
				{
					//Set the state of the buffer to ready
					buffer_states[j] = BUFFER_READY;
				}
			}
			else if (state == BUFFER_RUNNING)
			{
				if (cudaStreamQuery(streams[j]) == cudaSuccess)
				{
					//Set the state of the buffer to finished
					buffer_states[j] = BUFFER_FINISHED;
				}
			}
			else if (state == BUFFER_READY)
			{
				//Set the state of the buffer to ready
				buffer_states[j] = BUFFER_RUNNING;
				//Run the kernel
				CudaMain<<<cpuData.NUM_BLOCKS_DEV, cpuData.NUM_THREADS_PER_BLOCK, 0, streams[j]>>>(dev_blocks_buffers, j);
			}
		}
	}
	printf("Completed %d iterations successfully\n", ITERATIONS);

	CUDA_ERROR_CHECK(cudaDeviceSynchronize());

	//Put data onto hard drive
	for(int i = 0; i < cpuData.NUM_BLOCKS_DEV; i++)
	{
		*(cpuData.blockLoader->allBlocks() + cpuData.blockIds[i]) = cpuData.host_Blocks[i];
	}

	//Free the data from the device
	CUDA_ERROR_CHECK(cudaDeviceReset());

	cpuData.blockLoader->~BlockLoader();

	system("PAUSE");
	return 0;
}

__global__ void CudaMain(BlockOfNeurons* all_buffers, int buffer_id)
{

}
