#include "cpu\util\BlockLoader.h"

#pragma once
class CPUKernel
{
	static int* calculateNextBlocks(BlockOfNeurons* currentBlocks, int numDevBlocks);
};