#include "BlockLoader.h"
#include "MemoryMapped.h"
#include <stdio.h>

BlockLoader::BlockLoader(const char *filename)
{
	data = new MemoryMapped(filename, MemoryMapped::WholeFile, MemoryMapped::Normal);
	if (!data->isValid())
	{
		printf("Failed to open file %s\n", filename);
		return;
	}
}

BlockLoader::~BlockLoader()
{
	delete data;
}

BlockOfNeurons* BlockLoader::allBlocks()
{
	return (BlockOfNeurons*)data->getData();
}

int BlockLoader::numBlocks()
{
	return (int)(data->size() / sizeof(BlockOfNeurons));
}