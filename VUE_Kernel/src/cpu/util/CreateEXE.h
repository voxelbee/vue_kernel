#include "BlockLoader.h"
#include "MemoryMapped.h"
#include <stdio.h>

#pragma once
class CreateEXE
{
private:
	MemoryMapped * data;

	CreateEXE(CreateEXE& blockLoader);
	void operator=(CreateEXE& blockLoader);
public:
	CreateEXE(const char *filename);

	~CreateEXE();

	unsigned char* allData();

	long numBytes();
};