#include "MemoryMapped.h"

#pragma once
class BlockOfNeurons
{
public:
	static const short NUM_NEURONS_PER_BLOCK = 1024;
	static const short NUM_CONNECTIONS = 4;
	unsigned long ID;

	short inputs[NUM_NEURONS_PER_BLOCK*NUM_CONNECTIONS];
	char outerInputs[NUM_NEURONS_PER_BLOCK*NUM_CONNECTIONS];
	float weights[NUM_NEURONS_PER_BLOCK*NUM_CONNECTIONS];
	float output[NUM_NEURONS_PER_BLOCK];

	void print()
	{
		for (size_t i = 0; i < NUM_NEURONS_PER_BLOCK; i++)
		{
			for (size_t n = 0; n < NUM_CONNECTIONS; n++)
			{
				printf("Input: %d; Weights: %f; Outputs: %f; OtherIns: %d\n", inputs[n + i * NUM_CONNECTIONS], weights[n + i * NUM_CONNECTIONS], output[i], outerInputs[n + i * NUM_CONNECTIONS]);
			}
		}
	}
};

class BlockLoader
{
private:
	MemoryMapped* data;

public:
	BlockLoader(const char *filename);

	~BlockLoader();

	BlockOfNeurons* allBlocks();

	int numBlocks();

private:
	BlockLoader(BlockLoader& blockLoader);
	void operator=(BlockLoader& blockLoader);
};