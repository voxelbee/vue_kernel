#include <iostream>
#include "CreateEXE.h"
#include <string>

CreateEXE::CreateEXE(const char *filename)
{
	data = new MemoryMapped(filename, MemoryMapped::WholeFile, MemoryMapped::Normal);
	if (!data->isValid())
	{
		printf("Failed to open file %s\n", filename);
		return;
	}
}

CreateEXE::~CreateEXE()
{
	delete data;
}

unsigned char* CreateEXE::allData()
{
	return (unsigned char*)data->getData();
}

long CreateEXE::numBytes()
{
	return data->size();
}