#include <string>
#include "cpu/util/BlockLoader.h"

#pragma once
struct MainData
{
	int NUM_CONECTIONS;
	int NUM_THREADS_PER_BLOCK;
	unsigned long NUM_NEURONS;
	int NUM_THREADS;
	int NUM_BLOCKS_DEV;
	int TOTAL_NUM_BLOCKS;
	BlockLoader* blockLoader;
	BlockOfNeurons* host_Blocks;
	int* blockIds;
};

class CPUUtil
{
public:
	static void InitNetwork(BlockOfNeurons* allBlocks, int numBlocks, float startWeights, float startOutputs);

	static void CreateNetworkFile(int numBlocks, std::string fileName);

	static void ChangeMemAllocation(int minGB, int maxGB);

	static int CalculateThreadCount(unsigned long numberOfNeuronsW);

	static MainData Setup(unsigned long num_neurons, int num_Conections);

	static BlockLoader* LoadNetwork(std::string fileName, int totalNumBlocks);

	static void PrintDeviceProperties(cudaDeviceProp* devProp, int devCount);
};