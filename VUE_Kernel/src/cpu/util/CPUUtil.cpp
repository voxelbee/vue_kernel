#include <omp.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <string>
#include "cpu/util/CPUUtil.h"
#include "cpu/util/BlockLoader.h"
#include <windows.h>
#include <iostream>

void CPUUtil::InitNetwork(BlockOfNeurons* allBlocks, int numBlocks, float startWeights, float startOutputs)
{
#pragma omp parallel for
	for (int i = 0; i < numBlocks; i++)
	{
		BlockOfNeurons& block = *(allBlocks + i);
		block.ID = i;
		for (int n = 0; n < BlockOfNeurons::NUM_NEURONS_PER_BLOCK; n++)
		{
			block.output[n] = startOutputs;
			for (int c = 0; c < BlockOfNeurons::NUM_CONNECTIONS; c++)
			{
				block.weights[BlockOfNeurons::NUM_CONNECTIONS * n + c] = startWeights;
				block.inputs[BlockOfNeurons::NUM_CONNECTIONS * n + c] = (((n + c) % BlockOfNeurons::NUM_NEURONS_PER_BLOCK) + (rand() % 24)) % BlockOfNeurons::NUM_NEURONS_PER_BLOCK;
				if ((rand() / (float)RAND_MAX) > 0.97)
				{
					block.outerInputs[BlockOfNeurons::NUM_CONNECTIONS * n + c] = (n + c) % 6;
				}
				else
				{
					block.outerInputs[BlockOfNeurons::NUM_CONNECTIONS * n + c] = 0;
				}
			}
		}
	}
}

void CPUUtil::CreateNetworkFile(int numBlocks, std::string fileName)
{
	HANDLE _file = ::CreateFileA(fileName.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	LARGE_INTEGER size;
	size.QuadPart = numBlocks * sizeof(BlockOfNeurons);
	::SetFilePointerEx(_file, size, NULL, FILE_BEGIN);
	::SetEndOfFile(_file);
	::CloseHandle(_file);
}

void CPUUtil::ChangeMemAllocation(int minGB, int maxGB)
{
	HANDLE process = ::GetCurrentProcess();
	size_t min = (2 ^ 30) * minGB;
	size_t max = (2 ^ 30) * maxGB;
	::SetProcessWorkingSetSize(process, min, max);
}

int CPUUtil::CalculateThreadCount(unsigned long numberOfNeuronsW)
{
	int devCount;
	cudaGetDeviceCount(&devCount);
	unsigned long numberOfCores;

	for (int i = 0; i < devCount; i++)
	{
		cudaSetDevice(i);
		printf("\n#\n#\nCUDA Device #%d\n#\n#\n\n", i);

		cudaDeviceProp* deviceProps = new cudaDeviceProp();
		cudaGetDeviceProperties(deviceProps, i);
		int threads = deviceProps->maxThreadsPerBlock;
		int multiProcessorCount = deviceProps->multiProcessorCount;
		threads = deviceProps->maxThreadsPerBlock;

		numberOfCores = threads * multiProcessorCount;
		PrintDeviceProperties(deviceProps, devCount);

		if (numberOfCores > numberOfNeuronsW)
		{
			return numberOfNeuronsW;
		}
	}

	return numberOfCores;
}

BlockLoader* CPUUtil::LoadNetwork(std::string fileName, int totalNumBlocks)
{
	printf("Do you want a new network file? (y / n) ");
	std::string createNewFile;
	std::cin >> createNewFile;
	if (createNewFile == "y")
	{
		CreateNetworkFile(totalNumBlocks, fileName);
	}
	BlockLoader* blockLoader = new BlockLoader(fileName.c_str());
	if (createNewFile == "y")
	{
		InitNetwork(blockLoader->allBlocks(), blockLoader->numBlocks(), 0.5f, 1.0f);
	}
	return blockLoader;
}

MainData CPUUtil::Setup(unsigned long num_Neurons, int num_Conections)
{
	cudaSetDeviceFlags(cudaDeviceMapHost);
	SetErrorMode(SEM_FAILCRITICALERRORS);
	MainData data;
	data.NUM_NEURONS = num_Neurons;
	data.NUM_CONECTIONS = num_Conections;
	data.NUM_THREADS_PER_BLOCK = 1024;
	data.NUM_THREADS = CalculateThreadCount(data.NUM_NEURONS);
	data.NUM_BLOCKS_DEV = ceil(data.NUM_THREADS / data.NUM_THREADS_PER_BLOCK);
	data.blockLoader = LoadNetwork("C:/Temp/Blocks.bin", data.NUM_NEURONS / data.NUM_THREADS_PER_BLOCK);
	data.TOTAL_NUM_BLOCKS = data.blockLoader->numBlocks();
	data.host_Blocks = new BlockOfNeurons[data.NUM_BLOCKS_DEV];
	data.blockIds = new int[data.NUM_BLOCKS_DEV];

	for (int i = 0; i < data.NUM_BLOCKS_DEV; i++)
	{
		data.blockIds[i] = i;
	}

	for (int i = 0; i < data.NUM_BLOCKS_DEV; i++)
	{
		data.host_Blocks[i] = *(data.blockLoader->allBlocks() + data.blockIds[i]);
	}
	return data;
}

void CPUUtil::PrintDeviceProperties(cudaDeviceProp* devProp, int devCount)
{
	for (int i = 0; i < devCount; i++)
	{
		printf("Major revision number:         %d\n", devProp->major);
		printf("Minor revision number:         %d\n", devProp->minor);
		printf("Name:                          %s\n", devProp->name);
		printf("Total global memory:           %u\n", devProp->totalGlobalMem);
		printf("Total shared memory per block: %u\n", devProp->sharedMemPerBlock);
		printf("Total registers per block:     %d\n", devProp->regsPerBlock);
		printf("Warp size:                     %d\n", devProp->warpSize);
		printf("Maximum memory pitch:          %u\n", devProp->memPitch);
		printf("Maximum threads per block:     %d\n", devProp->maxThreadsPerBlock);
		printf("Warps per block:               %d\n", devProp->maxThreadsPerBlock / devProp->warpSize);
		for (int i = 0; i < 3; ++i)
		{
			if (i == 0)
			{
				printf("Maximum dimension x of block:  %d\n", i, devProp->maxThreadsDim[i]);
			}
			else if (i == 1)
			{
				printf("Maximum dimension y of block:  %d\n", i, devProp->maxThreadsDim[i]);
			}
			else if (i == 2)
			{
				printf("Maximum dimension z of block:  %d\n", i, devProp->maxThreadsDim[i]);
			}
		}
		for (int i = 0; i < 3; ++i)
		{
			if (i == 0)
			{
				printf("Maximum x dimension of grid:   %d\n", i, devProp->maxGridSize[i]);
			}
			else if (i == 1)
			{
				printf("Maximum y dimension of grid:   %d\n", i, devProp->maxGridSize[i]);
			}
			else if (i == 2)
			{
				printf("Maximum z dimension of grid:   %d\n", i, devProp->maxGridSize[i]);
			}
		}
		printf("Clock rate:                    %d\n", devProp->clockRate);
		printf("Total constant memory:         %u\n", devProp->totalConstMem);
		printf("Texture alignment:             %u\n", devProp->textureAlignment);
		printf("Concurrent copy and execution: %s\n", (devProp->deviceOverlap ? "Yes" : "No"));
		printf("Number of multiprocessors:     %d\n", devProp->multiProcessorCount);
		printf("Kernel execution timeout:      %s\n", (devProp->kernelExecTimeoutEnabled ? "Yes" : "No"));
		size_t mem;
		size_t total;
		cudaMemGetInfo(&mem, &total);
		printf("Amount of free memory:         %Id\n", mem);
		printf("Total memory:                  %Id\n", total);
		printf("asyncEngineCount:              %d\n", devProp->asyncEngineCount);
	}
}