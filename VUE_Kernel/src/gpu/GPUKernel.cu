#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "cpu/util/CPUUtil.h"
#include "GPUKernel.cuh"

__device__ void GPUKernel::RunNetwork(BlockOfNeurons* blocks, int buffer_id)
{
	__shared__ BlockOfNeurons* block;
	block = blocks + (blockIdx.x + buffer_id);

	GPUKernel::UpdateOutputs(block, blocks);
}

__device__ void GPUKernel::UpdateOutputs(BlockOfNeurons* block, BlockOfNeurons* allBlocks)
{
	int neuronIndex = threadIdx.x;

	float sum = 0;
	for (int i = 0; i < block->NUM_CONNECTIONS; i++)
	{
		sum += block->weights[i + neuronIndex * block->NUM_CONNECTIONS] * block->output[block->inputs[i + neuronIndex * block->NUM_CONNECTIONS]];
	}
	block->output[neuronIndex] = 1.0 / (1 + expf(-sum));
}