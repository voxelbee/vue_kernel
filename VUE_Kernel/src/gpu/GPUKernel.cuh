#include "cuda_runtime.h"
#include "device_launch_parameters.h"
class BlockOfNeurons;

#pragma once
class GPUKernel
{
public:
	static __device__ void UpdateOutputs(BlockOfNeurons* block, BlockOfNeurons* allBlocks);

	static __device__ void RunNetwork(BlockOfNeurons* blocks, int buffer_id);
};