#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#pragma once
class GPURand
{
public:
	static __device__ float RandomNum(unsigned int &m_w, unsigned int &m_z);

	static __device__ int RandomIntRange(int min, int max, unsigned int &m_w, unsigned int &m_z);

	static __device__ float RandomFloatRange(float min, float max, unsigned int &m_w, unsigned int &m_z);
};