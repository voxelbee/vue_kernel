#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "gpu/util/GPURand.cuh"

__device__ float GPURand::RandomNum(unsigned int &m_w, unsigned int &m_z)
{
	m_z = 36969 * (m_z & 65535) + (m_z >> 16);
	m_w = 18000 * (m_w & 65535) + (m_w >> 16);

	unsigned int t = ((m_z << 16) + m_w);
	return t / (float)UINT_MAX;
}

__device__ float GPURand::RandomFloatRange(float min, float max, unsigned int &m_w, unsigned int &m_z)
{
	float num = RandomNum(m_w, m_z);
	return num * min + (1 - num) * max;
}

__device__ int GPURand::RandomIntRange(int min, int max, unsigned int &m_w, unsigned int &m_z)
{
	float num = RandomNum(m_w, m_z);
	return (int)(num * min + (1 - num) * max);
}